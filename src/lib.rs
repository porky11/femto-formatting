#![deny(missing_docs)]

/*!
Available formatting:

* `normal`
* `*bold*`
* `/italic/`
* `` `mono` ``
* `^superscript^`
* `|subscript|`
* `^|small|^`
* `_underscore_`
* `~strikethrough~`

All formattings can be combined.
The formatting either ends after a second occurance of the same formatting character or when the line ends.
All characters after a `\` will be handled as normal.
*/

use femtovg::{Canvas, FontId, Paint, Path, Renderer};
use pukram_formatting::Formatting;

/// A font pack specifying different variants of a font.
#[derive(Clone)]
pub struct FontPack {
    /// The default font.
    pub default: FontId,
    /// The bold variant of the font.
    pub bold: FontId,
    /// The italic variant of the font.
    pub italic: FontId,
    /// The bold and italic variant of the font.
    pub bold_italic: FontId,
}

/// Render the specified `text_line` formatted.
pub fn render_text_formatted<R: Renderer>(
    x: f32,
    y: f32,
    w: f32,
    text_line: &str,
    formatting: Formatting,
    text_size: f32,
    fonts_default: &FontPack,
    fonts_mono: &FontPack,
    text_paint: &Paint,
    canvas: &mut Canvas<R>,
) -> usize {
    render_partial_text_formatted(
        x,
        y,
        w,
        text_line,
        text_line.len(),
        formatting,
        text_size,
        fonts_default,
        fonts_mono,
        text_paint,
        canvas,
    )
}

/// Render the specified `text_line` formatted.
pub fn render_partial_text_formatted<R: Renderer>(
    x: f32,
    y: f32,
    w: f32,
    text_line: &str,
    text_end: usize,
    mut formatting: Formatting,
    text_size: f32,
    fonts_default: &FontPack,
    fonts_mono: &FontPack,
    text_paint: &Paint,
    canvas: &mut Canvas<R>,
) -> usize {
    let mut line_start = 0;
    let mut start_offset = 0.0;
    let mut ignore_next = false;
    let mut lines = 0;

    let text_paint = &mut text_paint.clone();

    loop {
        text_paint.set_font(&[
            match (
                formatting.is_bold(),
                formatting.is_italic(),
                formatting.is_mono(),
            ) {
                (false, false, false) => fonts_default.default,
                (true, false, false) => fonts_default.bold,
                (false, true, false) => fonts_default.italic,
                (true, true, false) => fonts_default.bold_italic,
                (false, false, true) => fonts_mono.default,
                (true, false, true) => fonts_mono.bold,
                (false, true, true) => fonts_mono.italic,
                (true, true, true) => fonts_mono.bold_italic,
            },
        ]);

        let (font_size, height_offset) = match (formatting.is_top(), formatting.is_bottom()) {
            (false, false) => (text_size, 0.0),
            (true, false) => (text_size / 2.0, 0.0),
            (false, true) => (text_size / 3.0, text_size / 1.5),
            (true, true) => (text_size / 1.5, text_size / 3.0),
        };
        text_paint.set_font_size(font_size);

        let Ok(full_line_size) =
            canvas.break_text(w - x - start_offset, &text_line[line_start..], text_paint)
        else {
            break;
        };

        if full_line_size == 0 {
            break;
        }

        let mut full_line_end = line_start + full_line_size;

        if text_end < full_line_end {
            full_line_end = text_end;
        }

        let (line_size, partial) = if let Some(line_size) = text_line[line_start..full_line_end]
            .find(|c| {
                if ignore_next {
                    ignore_next = false;
                    false
                } else {
                    "*/`^|_~\\".contains(c)
                }
            }) {
            (line_size, true)
        } else {
            (full_line_end - line_start, false)
        };

        let y = y + lines as f32 * text_size + height_offset;
        let line_end = line_start + line_size;
        let mut shown_line = &text_line[line_start..line_end];
        if start_offset == 0.0 {
            shown_line = shown_line.trim_start();
        }

        let mut text_width = 0.0;
        if !shown_line.is_empty() {
            let _ = canvas.fill_text(x + start_offset, y, shown_line, text_paint);

            text_width = canvas
                .measure_text(x + start_offset, y, shown_line, text_paint)
                .unwrap()
                .width();

            if formatting.is_underscore() {
                let mut path = Path::new();
                path.move_to(x + start_offset, y + font_size);
                path.line_to(x + start_offset + text_width, y + font_size);
                canvas.stroke_path(&path, text_paint);
            }

            if formatting.is_strikethrough() {
                let mut path = Path::new();
                path.move_to(x + start_offset - font_size / 4.0, y + font_size / 1.5);
                path.line_to(
                    x + start_offset + text_width + font_size / 4.0,
                    y + font_size / 1.5,
                );
                canvas.stroke_path(&path, text_paint);
            }
        }

        if line_start == text_end {
            break;
        }

        line_start = line_end;

        if partial {
            start_offset += text_width;
            let c = text_line[line_start..].chars().next().unwrap();

            if c == '\\' {
                ignore_next = true;
            } else {
                formatting.apply(c);
            }

            line_start += 1;
        }

        if line_start == full_line_end {
            lines += 1;
            start_offset = 0.0;
        }
    }

    lines
}
